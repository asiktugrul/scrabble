# Scrabble-Game

## Important Note - Although Spring-Boot is not allowed, Why I choose it! 
Firstly, I designed all scrabble game in level of abstraction which is not dependent any framework. It means that it's easy to move this design to any place. (Look at ```tr.tugrul.game.core``` and ```tr.tugrul.game.scrabble``` packages)
After that, I researched about [dropwizard](https://www.dropwizard.io/en/stable/), [quarkus](https://quarkus.io/), [microprofile](https://microprofile.io/) and [spark-java](http://sparkjava.com/) frameworks; but I'm afraid these atomic frameworks have many bugs and their documentation pages are poor. In other words, these frameworks are not mature enough in order to develop fully dressed application including all tests. Also, their mocking support is so primitive and their libraries have many conflicts.

So that, I implemented my scrabble game design to spring-boot project.

## Pre-Requests
- Java 8 +
- Maven 3.6 + [Details](https://maven.apache.org/)
- H2 In memory db (No setup required) [Details](https://www.h2database.com/html/main.html)

## Setup And Running
- Just Tests\
```mvn test```
- All cycle and packaging\
```mvn clean install package```
- All cycle and packaging without tests\
```mvn clean install package -DskipTests=true```
- Run with spring-boot maven plugin\
```mvn spring-boot:run```
- Run Jar\
```java -jar target/scrabble-0.0.1-SNAPSHOT.jar```

## Logging
- scrabble.log
- Logback config ```/resources/logback-spring.xml```  or use ```--logging.config``` parameter in order to pass config as parameter
- ```<logger name="org.springframework.web.filter.CommonsRequestLoggingFilter" level="debug">``` is important that It's used for http request logging

## Endpoint Testing and Documentation
- Postman collection (Under ```/postman``` folder)

## Game Rules
- Every move must be into board bounds
- A word must be used just one time
- Words must be left to right or top to bottom direction
- Words must be in dictionary
- Every word must be connected

## TODO & Ideas
- Need to improve test coverage
- Need to improve edge case testes
- If high throughput is needed, JSON format may be changed
- If high throughput is needed, game rule validation machanism may be changed
