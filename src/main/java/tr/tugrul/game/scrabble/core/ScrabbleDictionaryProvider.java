package tr.tugrul.game.scrabble.core;

public interface ScrabbleDictionaryProvider {

    boolean find(String word);
}
