package tr.tugrul.game.scrabble.core;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ScrabbleSquare {

    public String character;
    public Integer positionX;
    public Integer positionY;
    public Boolean hasSet = Boolean.FALSE;
    public Double score = 0.0;
    public Integer turn = 1;

    public ScrabbleSquare(String character, Integer positionX, Integer positionY) {
        this.character = character;
        this.positionX = positionX;
        this.positionY = positionY;
    }

    public ScrabbleSquare(List<ScrabbleSquare> scrabbleSquares) {
    }

    public boolean getHasSet() {
        return !StringUtils.isEmpty(getCharacter());
    }
}
