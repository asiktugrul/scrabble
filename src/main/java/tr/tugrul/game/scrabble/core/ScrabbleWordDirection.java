package tr.tugrul.game.scrabble.core;

public enum ScrabbleWordDirection {
    LEFT_TO_RIGHT, TOP_TO_BOTTOM;
}
