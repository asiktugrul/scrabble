package tr.tugrul.game.scrabble.core;

import tr.tugrul.game.core.GameBuilder;

import java.util.List;
import java.util.Random;

public class ScrabbleGameBuilder implements GameBuilder {

    private ScrabbleGame scrabbleGame;

    public ScrabbleGameBuilder() {
        scrabbleGame = new ScrabbleGame();
    }

    public static ScrabbleGameBuilder instance() {
        return new ScrabbleGameBuilder();
    }

    public ScrabbleGameBuilder status(ScrabbleGameStatus scrabbleGameStatus) {
        this.scrabbleGame.setStatus(scrabbleGameStatus);
        return this;
    }

    public ScrabbleGameBuilder score(Double score) {
        this.scrabbleGame.setScore(score);
        return this;
    }

    public ScrabbleGameBuilder turn(Integer turn) {
        this.scrabbleGame.setTurn(turn);
        return this;
    }

    public ScrabbleGameBuilder moves(List<ScrabbleMove> moves) {
        this.scrabbleGame.setMoves(moves);
        return this;
    }

    public ScrabbleGameBuilder dimensions(Integer x, Integer y) {
        scrabbleGame.setDimensionX(x);
        scrabbleGame.setDimensionY(y);
        return this;
    }

    public ScrabbleGameBuilder dictionaryProvider(ScrabbleDictionaryProvider dictionaryProvider) {
        scrabbleGame.setScrabbleDictionaryProvider(dictionaryProvider);
        return this;
    }

    public ScrabbleGameBuilder scoreMapProvider(ScrabbleScoreMapProvider scoreMapProvider) {
        scrabbleGame.setScrabbleScoreMapProvider(scoreMapProvider);
        return this;
    }

    public ScrabbleGame build() {
        scrabbleGame.constructBoard();
        scrabbleGame.setId(Long.valueOf(new Random().nextInt(9999999)));
        return this.scrabbleGame;
    }
}
