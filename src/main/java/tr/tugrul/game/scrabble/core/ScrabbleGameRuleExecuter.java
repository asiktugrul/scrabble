package tr.tugrul.game.scrabble.core;

import tr.tugrul.game.core.GameRuleExecuter;
import tr.tugrul.game.scrabble.rule.*;
import tr.tugrul.game.core.GameRule;

import java.util.Arrays;
import java.util.List;

public class ScrabbleGameRuleExecuter extends GameRuleExecuter {

    private static List<GameRule> gameRules = Arrays.asList(new GameActiveRule(), new BoardBoundsRule(),
            new WordDirectionRule(), new DuplicateWordRule(), new WordChainingRule(), new WordMatchingRule());

    private static ScrabbleGameRuleExecuter scrabbleRuleExecuter;

    public static synchronized ScrabbleGameRuleExecuter instance() {
        if (scrabbleRuleExecuter == null) {
            scrabbleRuleExecuter = new ScrabbleGameRuleExecuter();
        }
        return scrabbleRuleExecuter;
    }

    private ScrabbleGameRuleExecuter() {
    }

    @Override
    public List<GameRule> getGameRules() {
        return gameRules;
    }
}
