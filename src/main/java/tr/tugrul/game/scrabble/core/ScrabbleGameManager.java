package tr.tugrul.game.scrabble.core;

import org.springframework.beans.BeanUtils;
import tr.tugrul.game.core.GameRuleException;
import tr.tugrul.game.core.GameManager;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static tr.tugrul.game.scrabble.core.ScrabbleGameStatus.*;

public class ScrabbleGameManager implements GameManager {

    public ScrabbleGameManager() {
    }

    public ScrabbleGame create(ScrabbleDictionaryProvider dictionaryProvider, Integer x, Integer y) {
        return ScrabbleGameBuilder
                .instance()
                .status(NOT_STARTED)
                .moves(new ArrayList<>())
                .dimensions(x, y)
                .dictionaryProvider(dictionaryProvider)
                .scoreMapProvider(new DefaultScrabbleScoreMap())
                .build();
    }

    public ScrabbleGame activate(ScrabbleGame game) {
        changeStatus(game, ScrabbleGameStatus.ACTIVE);
        return game;
    }

    public ScrabbleGame changeStatus(ScrabbleGame game, ScrabbleGameStatus newStatus) {
        game.setStatus(newStatus);
        return game;
    }

    public ScrabbleGame play(ScrabbleGame game, ScrabbleMove move, ScrabbleDictionaryProvider scrabbleDictionaryProvider) throws GameRuleException {
        if (game == null) {
            throw new GameRuleException("error.game.notFound");
        }
        game.bindDictionaryProvider(scrabbleDictionaryProvider);
        ScrabbleGameRuleExecuter.instance().run(game, move);
        apply(game, move);

        return game;
    }

    private void apply(ScrabbleGame game, ScrabbleMove move) {
        move.setTurn(game.getTurn());
        game.appendMove(move);

        move.getWords()
                .stream()
                .map(w -> w.getSquares())
                .forEach(s -> s.forEach(ss -> {
                    applyScoreAndBoardPositions(game, ss);
                }));

        game.nextTurn();
    }

    private void applyScoreAndBoardPositions(ScrabbleGame game, ScrabbleSquare scrabbleSquare) {
        ScrabbleScoreMapProvider scrabbleScoreMapProvider = game.getScrabbleScoreMapProvider();
        Double scoreForCharacter = scrabbleScoreMapProvider.forCharacter(scrabbleSquare.getCharacter());
        scrabbleSquare.setScore(scoreForCharacter);
        scrabbleSquare.setTurn(game.getTurn());
        game.getBoard()[scrabbleSquare.getPositionX()][scrabbleSquare.getPositionY()] = scrabbleSquare;

        game.incrementScoreBy(scoreForCharacter);
    }

    public List<ScrabbleMove> history(ScrabbleGame game, Long startIndex) throws GameRuleException {
        return game.getMoves().stream().skip(startIndex - 1).collect(Collectors.toList());
    }
}
