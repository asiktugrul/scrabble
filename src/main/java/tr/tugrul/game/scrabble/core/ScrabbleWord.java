package tr.tugrul.game.scrabble.core;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class ScrabbleWord {

    private List<ScrabbleSquare> squares = new ArrayList<>();

    public ScrabbleWord(List<ScrabbleSquare> squares) {
        this.squares = squares;
    }

    public String getWord() {
        return squares.stream()
                .map(s -> s.getCharacter())
                .collect(Collectors.joining(""));
    }

    public Double getScore() {
        return squares.stream()
                .map(ScrabbleSquare::getScore)
                .reduce(0.0, Double::sum);
    }
}
