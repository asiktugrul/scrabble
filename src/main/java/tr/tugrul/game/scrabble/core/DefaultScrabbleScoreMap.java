package tr.tugrul.game.scrabble.core;

import java.util.HashMap;
import java.util.Map;

public class DefaultScrabbleScoreMap implements ScrabbleScoreMapProvider {

    private static Map<String, Double> characterMap = new HashMap<String, Double>() {
        {
            put("a", 1.0);
            put("b", 1.0);
            put("c", 1.0);
            put("ç", 2.0);
            put("d", 1.0);
            put("e", 1.0);
            put("f", 1.0);
            put("g", 1.0);
            put("ğ", 5.0);
            put("h", 1.0);
            put("ı", 1.0);
            put("i", 1.0);
            put("j", 1.0);
            put("k", 1.0);
            put("l", 1.0);
            put("m", 1.0);
            put("n", 1.0);
            put("o", 1.0);
            put("ö", 1.0);
            put("p", 1.0);
            put("r", 1.0);
            put("s", 1.0);
            put("ş", 1.0);
            put("t", 1.0);
            put("u", 1.0);
            put("ü", 4.0);
            put("v", 1.0);
            put("y", 1.0);
            put("z", 1.0);
        }
    };

    @Override
    public Double forCharacter(String s) {
        return characterMap.get(s.toLowerCase());
    }
}
