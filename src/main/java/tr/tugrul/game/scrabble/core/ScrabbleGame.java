package tr.tugrul.game.scrabble.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import tr.tugrul.game.core.DimensionalGame;
import tr.tugrul.game.core.Game;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ScrabbleGame implements Game<ScrabbleMove>, DimensionalGame {

    private Long id;

    private ScrabbleGameStatus status;

    private Integer dimensionX = 0;
    private Integer dimensionY = 0;

    private ScrabbleSquare[][] board = new ScrabbleSquare[15][15];
    private List<ScrabbleMove> moves = new ArrayList<>();

    @JsonIgnore
    private transient ScrabbleDictionaryProvider scrabbleDictionaryProvider;

    @JsonIgnore
    private transient ScrabbleScoreMapProvider scrabbleScoreMapProvider;

    private Double score = 0.0;

    private Integer turn = 1;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public Double getScore() {
        return score;
    }

    public void setMoves(List<ScrabbleMove> moves) {
        this.moves = moves;
    }

    public void appendMove(ScrabbleMove move) {
        getMoves().add(move);
    }

    public void setScore(Double score) {
        this.score = score;
    }

    @Override
    public List<ScrabbleMove> getMoves() {
        return moves;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ScrabbleGameStatus getStatus() {
        return status;
    }

    public void setStatus(ScrabbleGameStatus status) {
        this.status = status;
    }

    public boolean isBoardEmpty() {
        return getMoves() == null || getMoves().isEmpty();
    }

    public ScrabbleSquare[][] getBoard() {
        return board;
    }

    public void setBoard(ScrabbleSquare[][] board) {
        this.board = board;
    }

    public void constructBoard() {
        setBoard(new ScrabbleSquare[getDimensionX()][getDimensionY()]);
        for (int i = 0; i < getDimensionX(); i++) {
            for (int j = 0; j < getDimensionY(); j++) {
                this.board[i][j] = new ScrabbleSquare("", i, j);
            }
        }
    }

    @Override
    public Integer getDimensionX() {
        return dimensionX;
    }

    @Override
    public Integer getDimensionY() {
        return dimensionY;
    }

    public void setDimensionX(Integer dimensionX) {
        this.dimensionX = dimensionX;
    }

    public void setDimensionY(Integer dimensionY) {
        this.dimensionY = dimensionY;
    }

    public void bindDictionaryProvider(ScrabbleDictionaryProvider dictionaryProvider) {
        setScrabbleDictionaryProvider(dictionaryProvider);
    }

    public void nextTurn() {
        setTurn(getTurn() + 1);
    }

    public void incrementScoreBy(Double score) {
        setScore(getScore() + score);
    }
}
