package tr.tugrul.game.scrabble.core;

public interface ScrabbleScoreMapProvider {

    Double forCharacter(String s);
}
