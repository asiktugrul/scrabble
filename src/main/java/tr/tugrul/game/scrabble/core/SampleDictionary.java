package tr.tugrul.game.scrabble.core;

import java.util.Arrays;
import java.util.List;

public class SampleDictionary implements ScrabbleDictionaryProvider {

    private static List<String> words = Arrays.asList("ALİ", "VELİ", "ÇAN", "AĞA", "CAN", "AMAN",
            "SAMAN", "KENAN", "DUMAN", "MANAV", "SINAV",
            "ŞINAV", " KALEM", "İNSAN", "ÖRÜMCEK");

    public SampleDictionary() {

    }

    public SampleDictionary(List<String> words) {
        this.words = words;
    }

    @Override
    public boolean find(String word) {
        return words.contains(word);
    }
}
