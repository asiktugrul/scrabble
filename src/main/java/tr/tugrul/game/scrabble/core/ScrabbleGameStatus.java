package tr.tugrul.game.scrabble.core;

public enum ScrabbleGameStatus {
    ACTIVE, EXPIRED, NOT_STARTED, OVER
}
