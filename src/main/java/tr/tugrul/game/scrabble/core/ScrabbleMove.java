package tr.tugrul.game.scrabble.core;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import tr.tugrul.game.core.Move;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ScrabbleMove implements Move {

    private List<ScrabbleWord> words = new ArrayList<>();

    private Integer turn = 1;

    public Double getScore() {
        return getWords()
                .stream()
                .map(ScrabbleWord::getScore)
                .reduce(0.0, Double::sum);
    }
}
