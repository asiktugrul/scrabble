package tr.tugrul.game.scrabble.rule;

import tr.tugrul.game.core.GameRuleException;
import tr.tugrul.game.scrabble.core.ScrabbleGame;
import tr.tugrul.game.scrabble.core.ScrabbleMove;
import tr.tugrul.game.scrabble.core.ScrabbleSquare;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class WordChainingRule extends ScrabbleGameRule {

    @Override
    public void execute(ScrabbleGame game, ScrabbleMove move) throws GameRuleException {
        validateNewMoveIsInternallyInChain(move);
        validateNewMoveIsInChainWithCurrentBoard(game, move);
    }

    private void validateNewMoveIsInChainWithCurrentBoard(ScrabbleGame game, ScrabbleMove move) {
        if (game.isBoardEmpty()) {
            return;
        }
        Long count = move.getWords()
                .stream()
                .filter(w -> 0 < w.getSquares()
                        .stream()
                        .filter(s -> {
                            ScrabbleSquare square = game.getBoard()[s.getPositionX()][s.getPositionY()];
                            if (square.getHasSet() && square.getCharacter().equals(s.getCharacter())) {
                                return true;
                            }
                            return false;
                        }).count()
                )
                .count();
        if (count.compareTo(0L) <= 0) {
            throw new GameRuleException("msg.error.chainNotFound");
        }
    }

    private void validateNewMoveIsInternallyInChain(ScrabbleMove move) {
        Integer howManyWords = move.getWords().size();
        Integer wordJointPoints = howManyWords - 1;
        List<ScrabbleSquare> allSquares = new ArrayList<>();
        move.getWords().stream()
                .forEach(w -> {
                    allSquares.addAll(w.getSquares());
                });

        Integer howManySquares = allSquares.size();
        Integer howManyDistinctSquares = new HashSet<ScrabbleSquare>(allSquares).size();

        if (howManySquares != howManyDistinctSquares + wordJointPoints) {
            throw new GameRuleException("msg.error.invalid.input");
        }
    }
}
