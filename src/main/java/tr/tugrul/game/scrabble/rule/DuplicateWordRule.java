package tr.tugrul.game.scrabble.rule;

import tr.tugrul.game.core.GameRuleException;
import tr.tugrul.game.scrabble.core.ScrabbleGame;
import tr.tugrul.game.scrabble.core.ScrabbleMove;
import tr.tugrul.game.scrabble.core.ScrabbleWord;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class DuplicateWordRule extends ScrabbleGameRule {
    @Override
    public void execute(ScrabbleGame game, ScrabbleMove move) throws GameRuleException {
        List<String> words = newlyWords(move);
        int newlyWordsCount = words.size();
        validateWordsDuplication(words, newlyWordsCount);

        if (game.isBoardEmpty()) {
            return;
        }

        List<String> wordsOnBoard = wordListOnBoard(game);

        int alreadyUsedWordsCount = wordsOnBoard.size();
        int expectedTotalWordsCount = newlyWordsCount + alreadyUsedWordsCount;
        wordsOnBoard.addAll(words);
        validateWordsDuplication(wordsOnBoard, expectedTotalWordsCount);
    }

    private void validateWordsDuplication(List<String> words, int count) {
        Set<String> distinctWords = new HashSet<>(words);
        if (count != distinctWords.size()) {
            throw new GameRuleException("error.game.duplicateWord");
        }
    }

    private List<String> newlyWords(ScrabbleMove move) {
        return move.getWords()
                .stream()
                .map(ScrabbleWord::getWord)
                .collect(Collectors.toList());
    }

    private List<String> wordListOnBoard(ScrabbleGame game) {
        return game.getMoves()
                .stream()
                .map(m -> m.getWords())
                .flatMap(l -> l.stream())
                .map(ScrabbleWord::getWord)
                .collect(Collectors.toList());
    }
}
