package tr.tugrul.game.scrabble.rule;

import tr.tugrul.game.core.GameRuleException;
import tr.tugrul.game.scrabble.core.ScrabbleGame;
import tr.tugrul.game.scrabble.core.ScrabbleMove;
import tr.tugrul.game.scrabble.core.ScrabbleWord;

import java.util.Arrays;
import java.util.List;

public class WordMatchingRule extends ScrabbleGameRule {

    @Override
    public void execute(ScrabbleGame game, ScrabbleMove move) throws GameRuleException {
        boolean noMatch = move.getWords()
                .stream()
                .map(ScrabbleWord::getWord)
                .filter(w -> !game.getScrabbleDictionaryProvider().find(w))
                .findFirst()
                .isPresent();

        if (noMatch) {
            throw new GameRuleException("error.game.wordNotF ound");
        }
    }
}
