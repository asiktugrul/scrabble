package tr.tugrul.game.scrabble.rule;

import tr.tugrul.game.core.GameRuleException;
import tr.tugrul.game.scrabble.core.ScrabbleGame;
import tr.tugrul.game.scrabble.core.ScrabbleMove;
import tr.tugrul.game.scrabble.core.ScrabbleSquare;

import java.util.function.Consumer;

public class BoardBoundsRule extends ScrabbleGameRule {

    @Override
    public void execute(ScrabbleGame game, ScrabbleMove move) throws GameRuleException {
        move.getWords()
                .forEach(e -> e.getSquares()
                        .forEach(validateBounds(game)));
    }

    private Consumer<ScrabbleSquare> validateBounds(ScrabbleGame game) {
        return s -> {
            squarePositionNotValid(s.getPositionX(), game.getDimensionX() - 1);
            squarePositionNotValid(s.getPositionY(), game.getDimensionY() - 1);
        };
    }

    private void squarePositionNotValid(Integer position, Integer boundedPosition) {
        boolean result = position == null || position < 0 || position > boundedPosition;
        if (result) {
            throw new GameRuleException("error.game.outOfBounds");
        }
    }
}
