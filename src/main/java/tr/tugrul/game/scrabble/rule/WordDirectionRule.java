package tr.tugrul.game.scrabble.rule;

import tr.tugrul.game.core.GameRuleException;
import tr.tugrul.game.scrabble.core.ScrabbleGame;
import tr.tugrul.game.scrabble.core.ScrabbleMove;
import tr.tugrul.game.scrabble.core.ScrabbleSquare;
import tr.tugrul.game.scrabble.core.ScrabbleWordDirection;

import java.util.List;
import java.util.function.Consumer;

public class WordDirectionRule extends ScrabbleGameRule {
    @Override
    public void execute(ScrabbleGame game, ScrabbleMove move) throws GameRuleException {
        move.getWords()
                .stream()
                .map(w -> w.getSquares())
                .forEach(checkDirectionsStrategyAndValidate());
    }

    private Consumer<List<ScrabbleSquare>> checkDirectionsStrategyAndValidate() {
        return s -> {
            if (s.get(0).getPositionX() < s.get(1).getPositionX()) {
                validateDirection(ScrabbleWordDirection.LEFT_TO_RIGHT, s);
            } else {
                validateDirection(ScrabbleWordDirection.TOP_TO_BOTTOM, s);
            }
        };
    }

    private void validateDirection(ScrabbleWordDirection direction, List<ScrabbleSquare> squares) {
        for (int i = 0; i < squares.size() - 1; i++) {
            if (ScrabbleWordDirection.LEFT_TO_RIGHT == direction) {
                if (isXDimensionsNotSequential(squares, i)
                        || isYDimensionsNotEquals(squares, i)) {
                    throw new GameRuleException("error.game.leftToRight.wordDirection");
                }
            } else if (isYDimensionsNotSequential(squares, i)
                    || isXDimensionsNotEquals(squares, i)) {
                throw new GameRuleException("error.game.topToBottom.wordDirection");
            }
        }
    }

    private boolean isXDimensionsNotEquals(List<ScrabbleSquare> squares, int i) {
        return !squares.get(i).getPositionX().equals(squares.get(i + 1).getPositionX());
    }

    private boolean isYDimensionsNotSequential(List<ScrabbleSquare> squares, int i) {
        return !squares.get(i).getPositionY().equals(squares.get(i + 1).getPositionY() + 1);
    }

    private boolean isYDimensionsNotEquals(List<ScrabbleSquare> squares, int i) {
        return !squares.get(i).getPositionY().equals(squares.get(i + 1).getPositionY());
    }

    private boolean isXDimensionsNotSequential(List<ScrabbleSquare> squares, int i) {
        return !squares.get(i).getPositionX().equals(squares.get(i + 1).getPositionX() - 1);
    }
}
