package tr.tugrul.game.scrabble.rule;

import tr.tugrul.game.scrabble.core.ScrabbleGame;
import tr.tugrul.game.scrabble.core.ScrabbleMove;
import tr.tugrul.game.core.GameRule;

public abstract class ScrabbleGameRule implements GameRule<ScrabbleGame, ScrabbleMove> {

    @Override
    public boolean canApply() {
        return true;
    }
}
