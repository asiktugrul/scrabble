package tr.tugrul.game.scrabble.rule;

import tr.tugrul.game.core.GameRuleException;
import tr.tugrul.game.scrabble.core.ScrabbleGame;
import tr.tugrul.game.scrabble.core.ScrabbleGameStatus;
import tr.tugrul.game.scrabble.core.ScrabbleMove;

public class GameActiveRule extends ScrabbleGameRule {
    @Override
    public void execute(ScrabbleGame game, ScrabbleMove move) throws GameRuleException {
        if (game.getStatus() != ScrabbleGameStatus.ACTIVE) {
            throw new GameRuleException("error.game.notActive");
        }
    }
}
