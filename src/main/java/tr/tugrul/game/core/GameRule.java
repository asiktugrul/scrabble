package tr.tugrul.game.core;

public interface GameRule<G extends Game, M extends  Move> {

    void execute(G game, M move) throws GameRuleException;

    boolean canApply();
}
