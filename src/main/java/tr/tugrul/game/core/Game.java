package tr.tugrul.game.core;

import java.util.List;

public interface Game<M extends Move> {

    Long getId();

    Double getScore();

    List<M> getMoves();
}
