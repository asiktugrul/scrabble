package tr.tugrul.game.core;

public interface GameRuleChecker {

    void run(Game game, Move move) throws GameRuleException;
}
