package tr.tugrul.game.core;

import java.util.List;

public abstract class GameRuleExecuter implements GameRuleChecker {

    public void run(Game game, Move move) throws GameRuleException {
        getGameRules()
                .stream()
                .filter(GameRule::canApply)
                .forEach(r -> {
                    r.execute(game, move);
                });
    }

    public abstract List<GameRule> getGameRules();
}
