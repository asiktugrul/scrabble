package tr.tugrul.game.core;

public interface GameBuilder<G extends Game> {

    G build();
}
