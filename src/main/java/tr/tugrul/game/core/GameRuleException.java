package tr.tugrul.game.core;

public class GameRuleException extends RuntimeException {

    public GameRuleException(String code) {
        super(code);
    }
}
