package tr.tugrul.game.core;

public interface DimensionalGame {

    Integer getDimensionX();

    Integer getDimensionY();
}
