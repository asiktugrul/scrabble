package tr.tugrul.game.web.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tr.tugrul.game.scrabble.core.*;
import tr.tugrul.game.web.dto.ScrabbleGameDTO;
import tr.tugrul.game.web.dto.ScrabbleMoveDTO;
import tr.tugrul.game.web.entity.ScrabbleGameStore;
import tr.tugrul.game.web.repository.ScrabbleGameRepository;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ScrabbleService {

    @Autowired
    private MockDistributedScrabbleCacheService scrabbleCacheService;

    @Autowired
    private ScrabbleGameManager scrabbleGameManager;

    @Autowired
    private DictionaryService dictionaryService;

    @Autowired
    private ScrabbleGameRepository scrabbleGameRepository;

    @Autowired
    private ObjectMapper objectMapper;

    public ScrabbleGameDTO start() {
        ScrabbleGame newGame = scrabbleGameManager.create(dictionaryService, 15, 15);
        scrabbleGameManager.activate(newGame);
        scrabbleCacheService.put(newGame.getId(), newGame);
        return convertToGameDTO(newGame);
    }

    public ScrabbleGameDTO play(Long id, ScrabbleMoveDTO move) {
        ScrabbleGame playedGame = scrabbleGameManager.play(scrabbleCacheService.get(id), move, dictionaryService);
        scrabbleCacheService.put(id, playedGame);
        return convertToGameDTO(playedGame);
    }

    public List<ScrabbleMoveDTO> status(Long id, Long fromIndex) {
        List<ScrabbleMove> history = scrabbleGameManager.history(find(id), fromIndex);
        return convertToScrabbleMoveDTOList(history);
    }

    public ScrabbleGameDTO gameOver(Long id) {
        ScrabbleGame finishedGame = scrabbleCacheService.get(id);
        scrabbleGameManager.changeStatus(finishedGame, ScrabbleGameStatus.OVER);
        saveToDB(id, finishedGame);
        scrabbleCacheService.remove(id);
        return convertToGameDTO(finishedGame);
    }

    private ScrabbleGame find(Long id) {
        ScrabbleGame scrabbleGame = scrabbleCacheService.get(id);
        if (scrabbleGame == null) {
            scrabbleGame = extractLastSavedGameState(id);
        }
        return scrabbleGame;
    }

    private void saveToDB(Long id, ScrabbleGame activeGame) {
        ScrabbleGameStore entity = null;
        try {
            entity = mapToScrabbleGameEntity(id, activeGame);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        scrabbleGameRepository.save(entity);
    }

    private ScrabbleGame extractLastSavedGameState(Long id) {
        Optional<ScrabbleGameStore> byGameId = scrabbleGameRepository.findByGameId(id);
        if (!byGameId.isPresent()) {
            throw new RuntimeException("Game can not find");
        }

        String state = byGameId.get().getState();
        ScrabbleGame savedGame = null;
        try {
            savedGame = objectMapper.readValue(state, ScrabbleGame.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return savedGame;
    }

    private ScrabbleGameStore mapToScrabbleGameEntity(Long id, ScrabbleGame activeGame) throws JsonProcessingException {
        ScrabbleGameStore store = new ScrabbleGameStore();
        store.setGameId(id);
        store.setTotalScore(activeGame.getScore());
        store.setTotalTurn(activeGame.getTurn());
        store.setStatus(ScrabbleGameStatus.OVER.toString());
        store.setState(objectMapper.writeValueAsString(activeGame));
        return store;
    }

    private ScrabbleGameDTO convertToGameDTO(ScrabbleGame newGame) {
        ScrabbleGameDTO newGameDTO = new ScrabbleGameDTO();
        BeanUtils.copyProperties(newGame, newGameDTO);
        return newGameDTO;
    }

    private List<ScrabbleMoveDTO> convertToScrabbleMoveDTOList(List<ScrabbleMove> history) {
        return history.stream().map(this::convertToScrabbleMoveDTO)
                .collect(Collectors.toList());
    }

    private ScrabbleMoveDTO convertToScrabbleMoveDTO(ScrabbleMove scrabbleMove) {
        ScrabbleMoveDTO scrabbleMoveDTO = new ScrabbleMoveDTO();
        BeanUtils.copyProperties(scrabbleMove, scrabbleMoveDTO);
        return scrabbleMoveDTO;
    }
}