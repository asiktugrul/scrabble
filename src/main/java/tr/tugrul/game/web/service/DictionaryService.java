package tr.tugrul.game.web.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import tr.tugrul.game.scrabble.core.ScrabbleDictionaryProvider;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class DictionaryService implements ScrabbleDictionaryProvider {

    @Value("classpath:data/scrabble_turkish_dictionary.txt")
    private Resource dictionaryFile;

    private static List<String> dictionary = new ArrayList<>();

    @PostConstruct
    public void init() {
        try {
            dictionary = Files.lines(dictionaryFile.getFile().toPath())
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean find(String word) {
        return Collections.binarySearch(dictionary, word.toLowerCase()) >= 0;
    }
}
