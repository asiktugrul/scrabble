package tr.tugrul.game.web.service;

import org.springframework.stereotype.Service;
import tr.tugrul.game.scrabble.core.ScrabbleGame;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class MockDistributedScrabbleCacheService {

    /* ASSUME that It's a global distributed cache */
    private static Map<Long, ScrabbleGame> gameStore = null;

    @PostConstruct
    public void init() {
        gameStore = new ConcurrentHashMap<>();
    }

    public void put(Long key, ScrabbleGame scrabbleGame) {
        gameStore.put(key, scrabbleGame);
    }

    public ScrabbleGame get(Long key) {
        return gameStore.get(key);
    }

    public ScrabbleGame remove(Long key) {
        return gameStore.remove(key);
    }
}
