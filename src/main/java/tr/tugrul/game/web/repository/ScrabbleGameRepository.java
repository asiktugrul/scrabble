package tr.tugrul.game.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tr.tugrul.game.web.entity.ScrabbleGameStore;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ScrabbleGameRepository extends JpaRepository<ScrabbleGameStore, UUID> {

    Optional<ScrabbleGameStore> findByGameId(Long gameId);
}
