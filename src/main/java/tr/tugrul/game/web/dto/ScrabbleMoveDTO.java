package tr.tugrul.game.web.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import tr.tugrul.game.scrabble.core.ScrabbleMove;

@Data
@NoArgsConstructor
public class ScrabbleMoveDTO extends ScrabbleMove {

}
