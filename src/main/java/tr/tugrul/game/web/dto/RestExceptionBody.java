package tr.tugrul.game.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RestExceptionBody implements Serializable {

    private Date timestamp;
    private Integer status;
    private String error;
    private String message;
}
