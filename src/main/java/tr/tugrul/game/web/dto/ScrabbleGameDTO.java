package tr.tugrul.game.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import tr.tugrul.game.scrabble.core.ScrabbleGameStatus;
import tr.tugrul.game.scrabble.core.ScrabbleSquare;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ScrabbleGameDTO implements Serializable {
    private Long id;
    private ScrabbleGameStatus status;
    private ScrabbleSquare[][] board;
    private Double score = 0.0;
    private Integer turn = 0;
}
