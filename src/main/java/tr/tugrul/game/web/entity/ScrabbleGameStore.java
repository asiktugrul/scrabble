package tr.tugrul.game.web.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table(name = "SCRABBLE_GAME_STORE")
public class ScrabbleGameStore extends BaseEntity {

    @Column(name = "GAME_ID")
    private Long gameId;

    @Column(name = "TOTAL_SCORE")
    private Double totalScore = 0.0;

    @Column(name = "TOTAL_TURN")
    private Integer totalTurn = 1;

    @Column(name = "STATUS")
    private String status;

    @Lob
    private String state;
}
