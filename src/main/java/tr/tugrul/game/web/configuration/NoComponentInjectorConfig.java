package tr.tugrul.game.web.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tr.tugrul.game.scrabble.core.ScrabbleGameManager;

@Configuration
public class NoComponentInjectorConfig {

    @Bean
    public ScrabbleGameManager scrabbleGameManager() {
        return new ScrabbleGameManager();
    }
}
