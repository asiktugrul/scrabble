package tr.tugrul.game.web.controller;

import org.springframework.http.ResponseEntity;
import tr.tugrul.game.web.dto.RestResponse;

public abstract class AbstractController {

    public ResponseEntity<?> ok(Object t) {
        return ResponseEntity.ok(new RestResponse(t));
    }

    public ResponseEntity<?> created(Object t) {
        return ResponseEntity.status(201).body(new RestResponse(t));
    }
}
