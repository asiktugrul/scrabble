package tr.tugrul.game.web.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import tr.tugrul.game.core.GameRuleException;
import tr.tugrul.game.web.dto.RestExceptionBody;
import tr.tugrul.game.web.dto.RestResponse;

import java.util.Calendar;

@ControllerAdvice
public class GlobalExceptionAdvisor {

    @ExceptionHandler({GameRuleException.class})
    public final ResponseEntity<?> handleException(Exception ex, WebRequest request) {
        RestResponse restResponse = buildErrorBody(ex);
        return ResponseEntity.status(400).body(restResponse);
    }

    private RestResponse buildErrorBody(Exception ex) {
        RestExceptionBody restExceptionBody = RestExceptionBody.builder()
                .error(ex.getMessage())
                .message(ex.getMessage())
                .status(400)
                .timestamp(Calendar.getInstance().getTime())
                .build();

        RestResponse restResponse = new RestResponse();
        restResponse.setError(restExceptionBody);
        return restResponse;
    }
}
