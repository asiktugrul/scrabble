package tr.tugrul.game.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tr.tugrul.game.scrabble.core.ScrabbleMove;
import tr.tugrul.game.web.dto.ScrabbleMoveDTO;
import tr.tugrul.game.web.service.ScrabbleService;

@RestController
@RequestMapping("/games/scrabble")
public class ScrabbleController extends AbstractController {

    @Autowired
    private ScrabbleService scrabbleGameService;

    @GetMapping("/{id}")
    public ResponseEntity<?> status(@PathVariable Long id, @RequestParam Long from) {
        return ok(scrabbleGameService.status(id, from));
    }

    @PostMapping("/start")
    public ResponseEntity<?> start() {
        return created(scrabbleGameService.start());
    }

    @PutMapping("/{id}/play")
    public ResponseEntity<?> play(@PathVariable Long id, @RequestBody ScrabbleMoveDTO scrabbleMoveDTO) {
        return ok(scrabbleGameService.play(id, scrabbleMoveDTO));
    }

    @PutMapping("/{id}/game-over")
    public ResponseEntity<?> gameOver(@PathVariable Long id) {
        return ok(scrabbleGameService.gameOver(id));
    }
}
