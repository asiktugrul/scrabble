package tr.tugrul.game.web.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import tr.tugrul.game.scrabble.core.*;
import tr.tugrul.game.scrabble.rule.ScrabbleWordTestHelper;
import tr.tugrul.game.web.dto.ScrabbleGameDTO;
import tr.tugrul.game.web.dto.ScrabbleMoveDTO;
import tr.tugrul.game.web.entity.ScrabbleGameStore;
import tr.tugrul.game.web.repository.ScrabbleGameRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static tr.tugrul.game.scrabble.core.ScrabbleGameStatus.ACTIVE;
import static tr.tugrul.game.scrabble.core.ScrabbleGameStatus.NOT_STARTED;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ScrabbleService.class)
class ScrabbleServiceTest {

    @Autowired
    private ScrabbleService scrabbleService;

    @MockBean
    private DictionaryService dictionaryService;

    @MockBean
    private ScrabbleGameRepository scrabbleGameRepository;

    @MockBean
    private ScrabbleGameManager scrabbleGameManager;

    @MockBean
    private MockDistributedScrabbleCacheService scrabbleCacheService;

    @MockBean
    private ObjectMapper objectMapper;

    @Test
    public void start_whenNewGameRequested_thenReturnActiveGame() {
        ScrabbleGame mockScrabbleGame = ScrabbleGameBuilder
                .instance()
                .status(NOT_STARTED)
                .build();

        when(scrabbleGameManager.create(dictionaryService, 15, 15))
                .thenReturn(mockScrabbleGame);

        ScrabbleGame mockActiveScrabbleGame = mockScrabbleGame;
        mockActiveScrabbleGame.setStatus(ScrabbleGameStatus.ACTIVE);

        when(scrabbleGameManager.activate(mockScrabbleGame))
                .thenReturn(mockActiveScrabbleGame);

        ScrabbleGameDTO scrabbleGameDTO = scrabbleService.start();
        verify(scrabbleCacheService).put(any(), eq(mockActiveScrabbleGame));
        assertNotNull(scrabbleGameDTO);
        assertNotNull(scrabbleGameDTO.getId());
        assertEquals(scrabbleGameDTO.getScore(), 0.0);
        assertEquals(scrabbleGameDTO.getTurn(), 1);
        assertEquals(ScrabbleGameStatus.ACTIVE.toString(), scrabbleGameDTO.getStatus().toString());
    }


    @Test
    public void firstMove_whenPlayed_thenReturnNextTurnGame() {
        ScrabbleMoveDTO scrabbleMoveDTO = new ScrabbleMoveDTO();
        ScrabbleWord can = ScrabbleWordTestHelper.canLeftToRight();
        scrabbleMoveDTO.setWords(Arrays.asList(can));

        ScrabbleGame expectedPreviousGame = ScrabbleGameBuilder
                .instance()
                .status(ACTIVE)
                .score(0.0)
                .turn(1)
                .build();

        ScrabbleGame expectedPlayedGame = ScrabbleGameBuilder
                .instance()
                .status(ACTIVE)
                .score(5.0)
                .turn(2)
                .moves(Arrays.asList(scrabbleMoveDTO))
                .build();

        when(scrabbleCacheService.get(-1L)).thenReturn(expectedPreviousGame);
        when(scrabbleGameManager.play(expectedPreviousGame, scrabbleMoveDTO, dictionaryService))
                .thenReturn(expectedPlayedGame);

        ScrabbleGameDTO scrabbleGameDTO = scrabbleService.play(-1L, scrabbleMoveDTO);

        verify(scrabbleCacheService).put(-1L, expectedPlayedGame);
        assertNotNull(scrabbleGameDTO);
        assertNotNull(scrabbleGameDTO.getId());
        assertEquals(scrabbleGameDTO.getScore(), 5.0);
        assertEquals(scrabbleGameDTO.getTurn(), 2);
        assertNotNull(scrabbleGameDTO.getBoard());
        assertEquals(ScrabbleGameStatus.ACTIVE.toString(), scrabbleGameDTO.getStatus().toString());
    }

    @Test
    public void status_whenActiveGameExist_thenReturnItsMoves() {
        ScrabbleMoveDTO scrabbleMoveDTO = new ScrabbleMoveDTO();
        ScrabbleWord can = ScrabbleWordTestHelper.canLeftToRight();
        scrabbleMoveDTO.setWords(Arrays.asList(can));

        ScrabbleGame expectedPlayedGame = ScrabbleGameBuilder
                .instance()
                .status(ACTIVE)
                .score(5.0)
                .turn(2)
                .moves(Arrays.asList(scrabbleMoveDTO))
                .build();

        when(scrabbleCacheService.get(-1L)).thenReturn(expectedPlayedGame);
        when(scrabbleGameManager.history(expectedPlayedGame, 1L))
                .thenReturn(Arrays.asList(scrabbleMoveDTO));

        List<ScrabbleMoveDTO> result = scrabbleService.status(-1L, 1L);

        assertEquals(result.size(), 1);
        assertEquals(result.get(0).getWords().get(0).getWord(), "CAN");
    }

    @Test
    public void status_whenGameAlreadyOver_thenReturnItsMovesFromDatabase() throws JsonProcessingException {
        ScrabbleMoveDTO scrabbleMoveDTO = new ScrabbleMoveDTO();
        ScrabbleWord can = ScrabbleWordTestHelper.canLeftToRight();
        scrabbleMoveDTO.setWords(Arrays.asList(can));

        ScrabbleGame expectedPlayedGame = ScrabbleGameBuilder
                .instance()
                .status(ACTIVE)
                .score(5.0)
                .turn(2)
                .moves(Arrays.asList(scrabbleMoveDTO))
                .build();

        ScrabbleGameStore store = new ScrabbleGameStore();
        String stateAsJSON = new ObjectMapper().writeValueAsString(expectedPlayedGame);
        store.setState(stateAsJSON);

        when(scrabbleCacheService.get(-1L)).thenReturn(null);
        when(scrabbleGameRepository.findByGameId(-1L)).thenReturn(Optional.of(store));
        when(objectMapper.readValue(stateAsJSON, ScrabbleGame.class)).thenReturn(expectedPlayedGame);
        when(scrabbleGameManager.history(expectedPlayedGame, 1L))
                .thenReturn(Arrays.asList(scrabbleMoveDTO));

        List<ScrabbleMoveDTO> result = scrabbleService.status(-1L, 1L);

        assertEquals(result.size(), 1);
        assertEquals(result.get(0).getWords().get(0).getWord(), "CAN");
    }

    @Test
    public void gameOver_whenTriggered_thenSaveGameToDatabaseAndReturnItsLastState() throws JsonProcessingException {
        ScrabbleMoveDTO scrabbleMoveDTO = new ScrabbleMoveDTO();
        ScrabbleWord can = ScrabbleWordTestHelper.canLeftToRight();
        scrabbleMoveDTO.setWords(Arrays.asList(can));

        ScrabbleGame activeGame = ScrabbleGameBuilder
                .instance()
                .dimensions(5, 5)
                .status(ACTIVE)
                .score(5.0)
                .turn(2)
                .moves(Arrays.asList(scrabbleMoveDTO))
                .build();

        when(scrabbleCacheService.get(1000L)).thenReturn(activeGame);

        ScrabbleGameDTO result = scrabbleService.gameOver(1000L);

        verify(scrabbleGameManager).changeStatus(activeGame, ScrabbleGameStatus.OVER);
        verify(scrabbleGameRepository).save(any());
        verify(objectMapper).writeValueAsString(activeGame);
        verify(scrabbleCacheService).remove(1000L);
        assertNotNull(result);
        assertEquals(result.getTurn(), 2);
        assertEquals(result.getScore(), 5);
    }
}