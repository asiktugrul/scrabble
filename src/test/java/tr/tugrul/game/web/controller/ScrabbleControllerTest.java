package tr.tugrul.game.web.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import tr.tugrul.game.scrabble.core.ScrabbleGameStatus;
import tr.tugrul.game.scrabble.core.ScrabbleSquare;
import tr.tugrul.game.scrabble.core.ScrabbleWord;
import tr.tugrul.game.scrabble.rule.ScrabbleWordTestHelper;
import tr.tugrul.game.web.dto.ScrabbleGameDTO;
import tr.tugrul.game.web.dto.ScrabbleMoveDTO;
import tr.tugrul.game.web.service.ScrabbleService;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(ScrabbleController.class)
class ScrabbleControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ScrabbleService scrabbleService;

    @Test
    public void gameStart_whenNewGameRequested_thenReturnNewGameAsJSON() throws Exception {
        ScrabbleGameDTO expectedDTOFromService = ScrabbleGameDTO.builder()
                .id(-1L)
                .score(0.0)
                .status(ScrabbleGameStatus.ACTIVE)
                .board(new ScrabbleSquare[3][3])
                .turn(0)
                .build();

        when(scrabbleService.start()).thenReturn(expectedDTOFromService);

        this.mockMvc.perform(
                post("/games/scrabble/start"))
                .andDo(print())
                .andExpect(status().is(201))
                .andExpect(jsonPath("$.data.id", is(-1)))
                .andExpect(jsonPath("$.data.status", is("ACTIVE")))
                .andExpect(jsonPath("$.data.score", is(0.0)))
                .andExpect(jsonPath("$.data.turn", is(0)))
                .andExpect(jsonPath("$.data.board", is(notNullValue())));
    }

    @Test
    public void firstMove_whenPlayedForTheGame_thenReturnModifiedGame() throws Exception {
        ScrabbleMoveDTO scrabbleMoveDTO = new ScrabbleMoveDTO();
        ScrabbleWord can = ScrabbleWordTestHelper.canLeftToRight();
        scrabbleMoveDTO.setWords(Arrays.asList(can));
        String scrabbleMoveDTOAsJSON = toJSON(scrabbleMoveDTO);

        ScrabbleSquare[][] board = new ScrabbleSquare[3][3];
        board[0][0] = new ScrabbleSquare("C", 0, 0);
        board[1][0] = new ScrabbleSquare("A", 1, 0);
        board[2][0] = new ScrabbleSquare("N", 2, 0);

        ScrabbleGameDTO expectedDTOFromService = ScrabbleGameDTO.builder()
                .id(99L)
                .score(0.0)
                .status(ScrabbleGameStatus.ACTIVE)
                .board(board)
                .turn(0)
                .build();

        when(scrabbleService.play(99L, scrabbleMoveDTO))
                .thenReturn(expectedDTOFromService);

        this.mockMvc.perform(
                put("/games/scrabble/99/play")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(scrabbleMoveDTOAsJSON))
                .andDo(print())
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.data.id", is(99)))
                .andExpect(jsonPath("$.data.status", is("ACTIVE")))
                .andExpect(jsonPath("$.data.score", is(0.0)))
                .andExpect(jsonPath("$.data.turn", is(0)))
                .andExpect(jsonPath("$.data.board[0][0]", is(notNullValue())))
                .andExpect(jsonPath("$.data.board[1][0]", is(notNullValue())))
                .andExpect(jsonPath("$.data.board[2][0]", is(notNullValue())))
                .andExpect(jsonPath("$.data.board[2][1]", is(nullValue())));
    }

    @Test
    public void gameOver_whenActiveGameFinished_thenReturnLatestGameStateWithStatusOver() throws Exception {
        ScrabbleMoveDTO scrabbleMoveDTO = new ScrabbleMoveDTO();
        ScrabbleWord can = ScrabbleWordTestHelper.canLeftToRight();
        scrabbleMoveDTO.setWords(Arrays.asList(can));
        String scrabbleMoveDTOAsJSON = toJSON(scrabbleMoveDTO);

        ScrabbleSquare[][] board = new ScrabbleSquare[3][3];
        board[0][0] = new ScrabbleSquare("C", 0, 0);
        board[1][0] = new ScrabbleSquare("A", 1, 0);
        board[2][0] = new ScrabbleSquare("N", 2, 0);

        ScrabbleGameDTO expectedDTOFromService = ScrabbleGameDTO.builder()
                .id(100L)
                .score(4.0)
                .status(ScrabbleGameStatus.OVER)
                .board(board)
                .turn(2)
                .build();

        when(scrabbleService.gameOver(100L))
                .thenReturn(expectedDTOFromService);

        this.mockMvc.perform(
                put("/games/scrabble/100/game-over")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(scrabbleMoveDTOAsJSON))
                .andDo(print())
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.data.id", is(100)))
                .andExpect(jsonPath("$.data.status", is("OVER")))
                .andExpect(jsonPath("$.data.score", is(4.0)))
                .andExpect(jsonPath("$.data.turn", is(2)))
                .andExpect(jsonPath("$.data.board[0][0]", is(notNullValue())))
                .andExpect(jsonPath("$.data.board[1][0]", is(notNullValue())))
                .andExpect(jsonPath("$.data.board[2][0]", is(notNullValue())));
    }

    @Test
    public void gameStatus_whenHistoryIndexSetAsOne_thenReturnAllScrabbleMoves() throws Exception {
        ScrabbleMoveDTO dtoForCan = new ScrabbleMoveDTO();
        ScrabbleWord can = ScrabbleWordTestHelper.canLeftToRight();
        dtoForCan.setWords(Arrays.asList(can));

        ScrabbleMoveDTO dtoForYan = new ScrabbleMoveDTO();
        ScrabbleWord yan = ScrabbleWordTestHelper.yanTopToBottom();
        dtoForYan.setWords(Arrays.asList(yan));

        List<ScrabbleMoveDTO> alreadyPlayedScrabbleMoves = Arrays.asList(dtoForCan, dtoForYan);
        String scrabbleMoveDTOListAsJSON = toJSON(alreadyPlayedScrabbleMoves);

        when(scrabbleService.status(9L, 1L))
                .thenReturn(alreadyPlayedScrabbleMoves);

        this.mockMvc.perform(
                get("/games/scrabble/9?from=1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(scrabbleMoveDTOListAsJSON))
                .andDo(print())
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.data[0].turn", is(1)))
                .andExpect(jsonPath("$.data[0].score", is(0.0)))
                .andExpect(jsonPath("$.data[0].words[0].squares[0].character", is("C")))
                .andExpect(jsonPath("$.data[0].words[0].squares[1].character", is("A")))
                .andExpect(jsonPath("$.data[0].words[0].squares[2].character", is("N")))
                .andExpect(jsonPath("$.data[1].words[0].squares[0].character", is("Y")))
                .andExpect(jsonPath("$.data[1].words[0].squares[1].character", is("A")))
                .andExpect(jsonPath("$.data[1].words[0].squares[2].character", is("N")));
    }

    private String toJSON(Object dto) {
        ObjectMapper om = new ObjectMapper();
        try {
            return om.writeValueAsString(dto);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}