package tr.tugrul.game.scrabble.rule;

import org.junit.jupiter.api.Test;
import tr.tugrul.game.core.GameRuleException;
import tr.tugrul.game.scrabble.core.ScrabbleGame;
import tr.tugrul.game.scrabble.core.ScrabbleGameStatus;
import tr.tugrul.game.scrabble.core.ScrabbleMove;
import tr.tugrul.game.scrabble.core.ScrabbleWord;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class WordDirectionRuleTest {

    @Test
    public void leftToRight_whenWordsHasEmptySquare_thenThrowExp() {
        ScrabbleGame active10x10Game = prepareActive10X10Game();
        ScrabbleWord masalWithEmptyPosition = ScrabbleWordTestHelper.masalLeftTorightWithEmptyPosition();

        ScrabbleMove invalidMove = ScrabbleMove.builder()
                .words(Arrays.asList(masalWithEmptyPosition))
                .build();

        ScrabbleGameRule wordDirectionRule = new WordDirectionRule();

        assertTrue(wordDirectionRule.canApply());
        assertThrows(GameRuleException.class, () ->
                        wordDirectionRule.execute(active10x10Game, invalidMove),
                "error.game.leftToRight.wordDirection");
    }

    @Test
    public void leftToRight_whenWordSquaresAreNotInLine_thenThrowExp() {
        ScrabbleGame active10x10Game = prepareActive10X10Game();
        ScrabbleWord hurmaUndirected = ScrabbleWordTestHelper.hurmaLeftToRightWithUndirectedPosition();

        ScrabbleMove invalidMove = ScrabbleMove.builder()
                .words(Arrays.asList(hurmaUndirected))
                .build();

        ScrabbleGameRule wordDirectionRule = new WordDirectionRule();

        assertTrue(wordDirectionRule.canApply());
        assertThrows(GameRuleException.class, () ->
                        wordDirectionRule.execute(active10x10Game, invalidMove),
                "error.game.leftToRight.wordDirection");
    }

    @Test
    public void leftToRight_whenWordsAreInLine_thenPassThrough() {
        ScrabbleGame active10x10Game = prepareActive10X10Game();
        ScrabbleWord canLeftToRight = ScrabbleWordTestHelper.canLeftToRight();

        ScrabbleMove invalidMove = ScrabbleMove.builder()
                .words(Arrays.asList(canLeftToRight))
                .build();

        ScrabbleGameRule wordDirectionRule = new WordDirectionRule();

        assertTrue(wordDirectionRule.canApply());
        assertDoesNotThrow(() ->
                wordDirectionRule.execute(active10x10Game, invalidMove)
        );
    }

    @Test
    public void topToBottom_whenWordsHasEmptySquare_thenThrowExp() {
        ScrabbleGame active10x10Game = prepareActive10X10Game();
        ScrabbleWord masalWithEmptyPosition = ScrabbleWordTestHelper.masalTopToBottomWithEmptyPosition();

        ScrabbleMove invalidMove = ScrabbleMove.builder()
                .words(Arrays.asList(masalWithEmptyPosition))
                .build();

        ScrabbleGameRule wordDirectionRule = new WordDirectionRule();

        assertTrue(wordDirectionRule.canApply());
        assertThrows(GameRuleException.class, () ->
                        wordDirectionRule.execute(active10x10Game, invalidMove),
                "error.game.topToBottom.wordDirection");
    }

    @Test
    public void topToBottom_whenWordSquaresAreNotInLine_thenThrowExp() {
        ScrabbleGame active10x10Game = prepareActive10X10Game();
        ScrabbleWord hurmaUndirected = ScrabbleWordTestHelper.hurmaTopToBottomWithUndirectedPosition();

        ScrabbleMove invalidMove = ScrabbleMove.builder()
                .words(Arrays.asList(hurmaUndirected))
                .build();

        ScrabbleGameRule wordDirectionRule = new WordDirectionRule();

        assertTrue(wordDirectionRule.canApply());
        assertThrows(GameRuleException.class, () ->
                        wordDirectionRule.execute(active10x10Game, invalidMove),
                "error.game.topToBottom.wordDirection");
    }

    @Test
    public void topToBottom_whenWordsAreInLine_thenPassThrough() {
        ScrabbleGame active10x10Game = prepareActive10X10Game();
        ScrabbleWord canTopToBottom = ScrabbleWordTestHelper.canTopToBottom();

        ScrabbleMove invalidMove = ScrabbleMove.builder()
                .words(Arrays.asList(canTopToBottom))
                .build();

        ScrabbleGameRule wordDirectionRule = new WordDirectionRule();

        assertTrue(wordDirectionRule.canApply());
        assertDoesNotThrow(() ->
                wordDirectionRule.execute(active10x10Game, invalidMove)
        );
    }

    private ScrabbleGame prepareActive10X10Game() {
        return ScrabbleGame.builder()
                .id(-1L)
                .dimensionX(10)
                .dimensionY(10)
                .status(ScrabbleGameStatus.ACTIVE)
                .build();
    }
}