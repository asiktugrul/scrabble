package tr.tugrul.game.scrabble.rule;


import org.junit.jupiter.api.Test;
import tr.tugrul.game.core.GameRuleException;
import tr.tugrul.game.scrabble.core.ScrabbleGame;
import tr.tugrul.game.scrabble.core.ScrabbleGameStatus;
import tr.tugrul.game.scrabble.core.ScrabbleMove;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class GameActiveRuleTest {

    @Test
    public void isPlayable_whenGameIsNotActive_thenThrowExp() {
        ScrabbleGameRule gameActiveRule = new GameActiveRule();

        ScrabbleGame notStartedGame = ScrabbleGame
                .builder()
                .status(ScrabbleGameStatus.NOT_STARTED)
                .build();

        ScrabbleMove emptyMove = ScrabbleMove
                .builder()
                .words(Collections.emptyList())
                .build();

        assertTrue(gameActiveRule.canApply());
        assertThrows(GameRuleException.class, () ->
                        gameActiveRule.execute(notStartedGame, emptyMove),
                "error.game.notActive"
        );
    }

    @Test
    public void isPlayable_whenGameIsActive_thenPassThrough() {
        ScrabbleGameRule gameActiveRule = new GameActiveRule();

        ScrabbleGame activeGame = ScrabbleGame
                .builder()
                .status(ScrabbleGameStatus.ACTIVE)
                .build();

        ScrabbleMove emptyMove = ScrabbleMove
                .builder()
                .words(Collections.emptyList())
                .build();

        assertTrue(gameActiveRule.canApply());
        assertDoesNotThrow(() ->
                gameActiveRule.execute(activeGame, emptyMove)
        );
    }
}