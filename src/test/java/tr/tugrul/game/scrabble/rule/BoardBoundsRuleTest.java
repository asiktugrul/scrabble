package tr.tugrul.game.scrabble.rule;

import org.junit.jupiter.api.Test;
import tr.tugrul.game.core.GameRuleException;
import tr.tugrul.game.scrabble.core.ScrabbleGame;
import tr.tugrul.game.scrabble.core.ScrabbleGameStatus;
import tr.tugrul.game.scrabble.core.ScrabbleMove;
import tr.tugrul.game.scrabble.core.ScrabbleWord;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class BoardBoundsRuleTest {

    @Test
    public void checkBounds_whenSquarePositionIsOutOfBoard_thenThrowExp() {
        ScrabbleGame active3x3Game = ScrabbleGame.builder()
                .id(-1L)
                .dimensionX(3)
                .dimensionY(3)
                .status(ScrabbleGameStatus.ACTIVE)
                .build();

        ScrabbleWord kanalTopToBottom = ScrabbleWordTestHelper.kanalTopToBottom();

        ScrabbleMove outOfBoundMove = ScrabbleMove.builder()
                .words(Arrays.asList(kanalTopToBottom))
                .build();

        ScrabbleGameRule boundsGameRule = new BoardBoundsRule();
        assertTrue(boundsGameRule.canApply());
        assertThrows(GameRuleException.class, () ->
                        boundsGameRule.execute(active3x3Game, outOfBoundMove),
                "error.game.outOfBounds");
    }

    @Test
    public void checkBounds_whenSquarePositionIsInBoard_thenPassThrough() {
        ScrabbleGame active3x3Game = ScrabbleGame.builder()
                .id(-1L)
                .dimensionX(3)
                .dimensionY(3)
                .status(ScrabbleGameStatus.ACTIVE)
                .build();

        ScrabbleWord yanTopToBottom = ScrabbleWordTestHelper.yanTopToBottom();

        ScrabbleMove outOfBoundMove = ScrabbleMove.builder()
                .words(Arrays.asList(yanTopToBottom))
                .build();

        ScrabbleGameRule boundsGameRule = new BoardBoundsRule();
        assertTrue(boundsGameRule.canApply());
        assertDoesNotThrow(() ->
                boundsGameRule.execute(active3x3Game, outOfBoundMove));
    }
}