package tr.tugrul.game.scrabble.rule;

import tr.tugrul.game.scrabble.core.ScrabbleSquare;
import tr.tugrul.game.scrabble.core.ScrabbleWord;

import java.util.Arrays;

public class ScrabbleWordTestHelper {

    public static ScrabbleWord canLeftToRight() {
        ScrabbleSquare c = new ScrabbleSquare("C", 0, 0);
        ScrabbleSquare a = new ScrabbleSquare("A", 1, 0);
        ScrabbleSquare n = new ScrabbleSquare("N", 2, 0);
        return new ScrabbleWord(Arrays.asList(c, a, n));
    }

    public static ScrabbleWord canTopToBottom() {
        ScrabbleSquare c = new ScrabbleSquare("C", 2, 2);
        ScrabbleSquare a = new ScrabbleSquare("A", 2, 1);
        ScrabbleSquare n = new ScrabbleSquare("N", 2, 0);
        return new ScrabbleWord(Arrays.asList(c, a, n));
    }

    public static ScrabbleWord yanTopToBottom() {
        ScrabbleSquare y = new ScrabbleSquare("Y", 2, 2);
        ScrabbleSquare a = new ScrabbleSquare("A", 2, 1);
        ScrabbleSquare n = new ScrabbleSquare("N", 2, 0);
        return new ScrabbleWord(Arrays.asList(y, a, n));
    }

    public static ScrabbleWord kanalTopToBottom() {
        ScrabbleSquare k = new ScrabbleSquare("K", 2, 4);
        ScrabbleSquare a = new ScrabbleSquare("A", 2, 3);
        ScrabbleSquare n = new ScrabbleSquare("N", 2, 2);
        ScrabbleSquare $a = new ScrabbleSquare("A", 2, 1);
        ScrabbleSquare l = new ScrabbleSquare("L", 2, 0);
        return new ScrabbleWord(Arrays.asList(k, a, n, $a, l));
    }

    public static ScrabbleWord masalTopToBottomWithEmptyPosition() {
        ScrabbleSquare m = new ScrabbleSquare("M", 2, 10);
        ScrabbleSquare a = new ScrabbleSquare("A", 2, 3);
        ScrabbleSquare s = new ScrabbleSquare("S", 2, 2);
        ScrabbleSquare $a = new ScrabbleSquare("A", 2, 1);
        ScrabbleSquare l = new ScrabbleSquare("L", 2, 0);
        return new ScrabbleWord(Arrays.asList(m, a, s, $a, l));
    }

    public static ScrabbleWord hurmaTopToBottomWithUndirectedPosition() {
        ScrabbleSquare h = new ScrabbleSquare("H", 2, 4);
        ScrabbleSquare u = new ScrabbleSquare("U", 2, 3);
        ScrabbleSquare r = new ScrabbleSquare("R", 3, 2);
        ScrabbleSquare m = new ScrabbleSquare("M", 2, 1);
        ScrabbleSquare a = new ScrabbleSquare("A", 2, 0);
        return new ScrabbleWord(Arrays.asList(h, u, r, m, a));
    }

    public static ScrabbleWord masalLeftTorightWithEmptyPosition() {
        ScrabbleSquare m = new ScrabbleSquare("M", 0, 2);
        ScrabbleSquare a = new ScrabbleSquare("A", 1, 2);
        ScrabbleSquare s = new ScrabbleSquare("S", 2, 2);
        ScrabbleSquare $a = new ScrabbleSquare("A", 6, 2);
        ScrabbleSquare l = new ScrabbleSquare("L", 7, 2);
        return new ScrabbleWord(Arrays.asList(m, a, s, $a, l));
    }

    public static ScrabbleWord hurmaLeftToRightWithUndirectedPosition() {
        ScrabbleSquare h = new ScrabbleSquare("H", 0, 0);
        ScrabbleSquare u = new ScrabbleSquare("U", 1, 1);
        ScrabbleSquare r = new ScrabbleSquare("R", 2, 2);
        ScrabbleSquare m = new ScrabbleSquare("M", 3, 3);
        ScrabbleSquare a = new ScrabbleSquare("A", 4, 4);
        return new ScrabbleWord(Arrays.asList(h, u, r, m, a));
    }

    public static ScrabbleWord otomobilTopToBottom() {
        ScrabbleSquare o = new ScrabbleSquare("O", 6, 0);
        ScrabbleSquare t = new ScrabbleSquare("T", 6, 1);
        ScrabbleSquare $o = new ScrabbleSquare("O", 6, 2);
        ScrabbleSquare m = new ScrabbleSquare("M", 6, 3);
        ScrabbleSquare $$o = new ScrabbleSquare("O", 6, 4);
        ScrabbleSquare b = new ScrabbleSquare("B", 6, 5);
        ScrabbleSquare i = new ScrabbleSquare("İ", 6, 6);
        ScrabbleSquare l = new ScrabbleSquare("L", 6, 7);
        return new ScrabbleWord(Arrays.asList(o, t, $o, m, $$o, b, i, l));
    }
}
