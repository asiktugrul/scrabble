package tr.tugrul.game.scrabble.rule;

import org.junit.jupiter.api.Test;
import tr.tugrul.game.core.GameRuleException;
import tr.tugrul.game.scrabble.core.*;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class WordMatchingRuleTest {

    @Test
    public void wordMatch_whenWordIsInDictionary_thenPassThrough() {
        ScrabbleGame active10x10Game = prepareActive10X10Game();
        ScrabbleWord otomobilTopToBottom = ScrabbleWordTestHelper.otomobilTopToBottom();

        ScrabbleMove singleMatchedWordMove = ScrabbleMove.builder()
                .words(Arrays.asList(otomobilTopToBottom))
                .build();

        ScrabbleGameRule wordMatchingRule = new WordMatchingRule();

        assertTrue(wordMatchingRule.canApply());
        assertDoesNotThrow(() ->
                wordMatchingRule.execute(active10x10Game, singleMatchedWordMove)
        );
    }

    @Test
    public void wordMatch_whenWordIsNotInDictionary_thenThrowExp() {
        ScrabbleGame active10x10Game = prepareActive10X10Game();
        ScrabbleWord canTopToBottom = ScrabbleWordTestHelper.canTopToBottom();

        ScrabbleMove singleNotMatchedWordMove = ScrabbleMove.builder()
                .words(Arrays.asList(canTopToBottom))
                .build();

        ScrabbleGameRule wordMatchingRule = new WordMatchingRule();

        assertTrue(wordMatchingRule.canApply());
        assertThrows(GameRuleException.class, () ->
                        wordMatchingRule.execute(active10x10Game, singleNotMatchedWordMove),
                "error.game.wordNotFound"
        );
    }

    private ScrabbleGame prepareActive10X10Game() {
        return ScrabbleGame.builder()
                .id(-1L)
                .dimensionX(10)
                .dimensionY(10)
                .status(ScrabbleGameStatus.ACTIVE)
                .scrabbleDictionaryProvider(new SampleDictionary(Arrays.asList("OTOMOBİL", "EMLAK", "TAMİR")))
                .build();
    }
}