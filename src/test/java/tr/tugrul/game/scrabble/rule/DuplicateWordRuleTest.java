package tr.tugrul.game.scrabble.rule;

import org.junit.jupiter.api.Test;
import tr.tugrul.game.core.GameRuleException;
import tr.tugrul.game.scrabble.core.ScrabbleGame;
import tr.tugrul.game.scrabble.core.ScrabbleGameStatus;
import tr.tugrul.game.scrabble.core.ScrabbleMove;
import tr.tugrul.game.scrabble.core.ScrabbleWord;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class DuplicateWordRuleTest {

    @Test
    public void differentDirection_whenDuplicateWordFound_thenThrowExp() {
        ScrabbleGame active3x3Game = ScrabbleGame.builder()
                .id(-1L)
                .dimensionX(3)
                .dimensionY(3)
                .status(ScrabbleGameStatus.ACTIVE)
                .build();

        ScrabbleWord canLeftToRight = ScrabbleWordTestHelper.canLeftToRight();
        ScrabbleWord canTopToBottom = ScrabbleWordTestHelper.canTopToBottom();

        ScrabbleMove duplicateWordMove = ScrabbleMove.builder()
                .words(Arrays.asList(canLeftToRight, canTopToBottom))
                .build();

        ScrabbleGameRule duplicateWordRule = new DuplicateWordRule();

        assertTrue(duplicateWordRule.canApply());
        assertThrows(GameRuleException.class, () ->
                        duplicateWordRule.execute(active3x3Game, duplicateWordMove),
                "error.game.duplicateWord"
        );
    }

    @Test
    public void differentDirection_whenDuplicateWordNotFound_thenPassThrough() {
        ScrabbleGame active3x3Game = ScrabbleGame.builder()
                .id(-1L)
                .dimensionX(3)
                .dimensionY(3)
                .status(ScrabbleGameStatus.ACTIVE)
                .build();

        ScrabbleWord canLeftToRight = ScrabbleWordTestHelper.canLeftToRight();
        ScrabbleWord yanTopToBottom = ScrabbleWordTestHelper.yanTopToBottom();

        ScrabbleMove duplicateWordMove = ScrabbleMove.builder()
                .words(Arrays.asList(canLeftToRight, yanTopToBottom))
                .build();

        ScrabbleGameRule duplicateWordRule = new DuplicateWordRule();

        assertTrue(duplicateWordRule.canApply());
        assertDoesNotThrow(() ->
                duplicateWordRule.execute(active3x3Game, duplicateWordMove)
        );
    }
}